# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event3

class ShootWithEvent(Event3):
    NAME = "shoot-on"

    def perform(self):
        self.inform("shoot-on")